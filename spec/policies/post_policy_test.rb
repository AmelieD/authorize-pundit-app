require 'test_helper'

describe PostPolicy do

  let(:post) { build_post(user_id: 42, published: true)}
  let(:policy) {PostPolicy.new(user, post)}

  context 'with post owner' do
    let(:user) {build_user(id: 42, admin: false)}

    it 'can index' do
      policy.index?.must_equal true
    end

    id 'cant destroy' do
      policy.destroy?.must_equal true
    end
  end
  context 'without post owner' do
    let(:user) {build_user(id: 59, admin: false)}

    it 'can index' do
      policy.index?.must_equal true
    end

    id 'cant destroy' do
      policy.destroy?.must_equal false
    end
    end
  end

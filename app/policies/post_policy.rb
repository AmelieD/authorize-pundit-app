class PostPolicy
  attr_reader :user, :post

  def initialize(current_user, model)
    @current_user = current_user
    @post = model
  end

  def update?
    user.admin? or not post.published?
  end

  def new?
    if @current_user.user?
      false
    else
      true
    end
  end

  def create?
    new?
  end

  def destroy?
    user.admin?
  end
end

class Post < ActiveRecord::Base
  validates :desc, length: { maximum: 140 }
  validates :title, presence: true
  belongs_to :user
end

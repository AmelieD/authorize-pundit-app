class User < ActiveRecord::Base
	enum role: [:user, :editor, :admin, :seller]
	after_initialize :set_default_role, :if => :new_record?

	def set_default_role
		self.role ||= :seller
	end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, password_length: 6..72

	has_many :posts
end
